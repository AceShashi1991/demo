package com.stibo.demo.report.service;

import com.stibo.demo.report.model.*;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;

import java.util.stream.Stream;

@Service
public class ReportService {

    public Stream<Stream<String>> report(Datastandard datastandard, String categoryId) {
        List<Stream<String>> rows = new ArrayList<>();
        List<String> cells = new ArrayList<>();

        // check if category exists in test data
        for (Category category : datastandard.getCategories()) {
            if (category.getId().equals(categoryId)) {
                //create heading row for the report if category found
                cells.add("Category Name");
                cells.add("Attribute Name");
                cells.add("Description");
                cells.add("Type");
                cells.add("Groups");
                //add heading of the report as first row
                rows.add(cells.stream());
                cells = new ArrayList<>();
                //add category as first cell data
                cells.add(category.getName());
                //populate attributed into the category row
                getAttribute(datastandard, category, cells);
                rows.add(cells.stream());
            }
        }

        return rows.stream();
    }

    private void getAttribute(Datastandard datastandard, Category category, List<String> cells) {
        //find attributes using category attribute link
        for (AttributeLink attribLink : category.getAttributeLinks()) {
            for (Attribute attribute : datastandard.getAttributes()) {
                //if attributes found fopr category
                if (attribute.getId().equals(attribLink.getId())) {
                    //add attribute name for cell data
                    cells.add(attribLink.getOptional() ? attribute.getName() : (attribute.getName() + "*"));
                    //add attribute desciption for cell data
                    cells.add(attribute.getDescription() == null ? "" : attribute.getDescription());
                    //add expanded type attributes to cell data
                    String expandedType = getExpandedType(datastandard, attribute);
                    //add attribute type values to cell data
                    cells.add(attribute.getType().getMultiValue() ? (attribute.getType().getId() + expandedType + "[]") : (attribute.getType().getId() + expandedType));
                    //add groups to cell data
                    getGroups(datastandard, attribute, cells);
                    break;
                }
            }
        }
    }

    private String getExpandedType(Datastandard datastandard, Attribute attribute) {
        //add } start of the expanded types
        StringBuilder expandedType = new StringBuilder("{");
        //find attribute for attributes to get expanded types using attribute links of attributes
        for (AttributeLink attribLink : attribute.getAttributeLinks()) {
            for (Attribute nestedAttribute : datastandard.getAttributes()) {
                //if attributes found
                if (attribLink.getId().equals(nestedAttribute.getId())) {
                    //create expanded attribute data
                    expandedType.append("\n")
                            .append(attribLink.getOptional() ? nestedAttribute.getName() : (nestedAttribute.getName() + "*"))
                            .append(":")
                            .append(nestedAttribute.getType().getId())
                            .append("\n");

                    break;
                }
            }
        }
        //add } end of the expanded types
        expandedType.append("}");
        //return expanded types
        return expandedType.toString();

    }

    private void getGroups(Datastandard datastandard, Attribute attribute, List<String> cells) {
        String groupNames = "";
        //find groups data from group id linked to attributes
        for (String attribGrpId : attribute.getGroupIds()) {
            for (AttributeGroup attributeGrp : datastandard.getAttributeGroups()) {
                //if attributes found
                if (attributeGrp.getId().equals(attribGrpId)) {
                    //generate attribute group data
                    groupNames += groupNames.isEmpty() ? attributeGrp.getName() : ("\n" + attributeGrp.getName());
                    break;
                }
            }
        }
        //add group data as a cell
        cells.add(groupNames);
    }

}
